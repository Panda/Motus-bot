# import mastodon
# import os
# import time
# import mimetypes
# from urllib.request import urlretrieve
# from PIL import Image
#
# from TootHTMLParser import TootHTMLParser
# import Consts
# from BotListener import BotListener
# import Log

import os
import time
import logging
from html.parser import HTMLParser

import mastodon
from unidecode import unidecode

import params


class Parser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.txt = ""

    def handle_data(self, data):
        data = (
            str(data).lower().replace("'", " ").replace(".", " ").replace(",", " ").replace("?", " ").replace("!", " ")
        )
        self.txt += unidecode(data)

    def handle_endtag(self, tag):
        if tag == "p":
            self.txt += "\n\n"

    def handle_starttag(self, tag, attrs):
        if tag == "br":
            self.txt += "\n"

    def get_text(self):
        txt = self.txt
        self.txt = ""
        return txt.strip()


class BotListener(mastodon.StreamListener):
    def __init__(self, game):
        self.game = game
        self.parser = Parser()

    def on_notification(self, notification):
        if notification["type"] == "mention":
            status = notification["status"]
            content = str(status["content"])
            self.parser.feed(content)
            content = self.parser.get_text()
            logging.info(f"toot is {content}")

            if status["visibility"] == "direct" and "motus" in content.split():
                logging.info("Private game")
                self.game.new_game(status["account"]["acct"])
            else:
                self.game.answer(content, status)


# The bot that rocks. Can listen to events and publish toots !
class Bot:
    def __init__(self, game):
        self.game = game
        self.game.bot = self
        self.listener = BotListener(game)

        api_url = ""
        max_toot_chars = -1
        with open(params.api_url_file, "r") as f:
            api_url = f.readline().strip()
            max_toot_chars = int(f.readline().strip())
        self.api_mastodon = mastodon.Mastodon(
            client_id=params.appCred, access_token=params.accountCred, api_base_url=api_url
        )
        logging.info(
            f"Bot connected to it's account on {api_url}. Maximum number of characters per toot is {max_toot_chars}."
        )
        self.max_toot_chars = max_toot_chars

    def toot(self, text, status, media=False, is_direct=False):
        logging.info(f"answering  : {text}")
        at_user = ""
        if status is not None:
            at_user = "@" + status["account"]["acct"]
            text = at_user + "\n" + text
        # else:
        #     text = "@Panda_Fuligineux@mastodon.xyz " + text

        if is_direct:
            visibility = "direct"
        else:
            visibility = "unlisted"

        if media:
            media = self.api_mastodon.media_post(params.congratulation_media)
            media = [media["id"]]
        else:
            media = None

        reply = None
        if status is not None:
            reply = status["id"]
        status = self.api_mastodon.status_post(
            text[: self.max_toot_chars - 1],
            in_reply_to_id=reply,
            visibility=visibility,
            media_ids=media,
            sensitive=True,
        )
        logging.info(text)
        return status

    # We just stream the notifications. Never ending function (in theory)
    def listen(self):
        logging.info("Now listening to notifications...")
        connection_handle = self.api_mastodon.stream_user(self.listener, run_async=True, reconnect_async=True)
        while connection_handle.is_alive():
            time.sleep(1)

    def emergency_contact(self):
        contact = ""
        with open(params.emergencyContactFile) as f:
            contact = f.readline().strip()
        if len(contact) > 1:
            self.api_mastodon.status_post(f"@{contact} {params.emergencyMessage}", visibility="direct")
