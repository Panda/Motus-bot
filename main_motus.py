import os
import sys
import time
import pathlib
import logging
import traceback

import params
from motus_game import MotusGame
from bot import Bot

# Debug mode !
params.debug = False
params.debug_commands_file = None

# Logging configuration
# Creation of a log file.
pathlib.Path(params.logDir).mkdir(parents=True, exist_ok=True)

log_format = "%(asctime)s - [%(levelname)s] : %(message)s"
if params.debug:
    # In debug mode we always create a new log file with all messages printed, and also print in stdout.
    logging.basicConfig(
        level=logging.DEBUG,
        format=log_format,
        handlers=[logging.FileHandler(params.log, mode="w"), logging.StreamHandler(sys.stdout)],
    )
else:
    # In normal mode, we append the log file with all messages but debug ones.
    logging.basicConfig(
        level=logging.INFO, format=log_format, handlers=[logging.FileHandler(params.log)],
    )


logging.info("Starting program.")
logging.debug(f"Debug mode is {params.debug}.")
logging.debug(f"Debug commands file is {params.debug_commands_file}")


# # We create the game, which is independant of the bot. We have to link it to the bot to publish toot and interact with Mastodon.
game = MotusGame()
logging.info("Game created.")


# If we do not find the access tokens, we write an error to the log file and quit.
if not os.path.exists(params.appCred) or not os.path.exists(params.accountCred):
    logging.error(
        f"{params.appCred} or {params.accountCred} files not found. You should create your access tokens using register.py before trying to run the bot."
    )
    sys.exit()

# We create the bot object and link it to the listener.
bot = Bot(game)
game.run()

# listen indefinitely and emergency contact ?
emergency_contact_contacted = False
# bot.listen()
while True:
    try:
        logging.info("Listening...")
        bot.listen()
    except Exception as e:
        logging.error(traceback.format_exc())

        if not emergency_contact_contacted:
            bot.emergency_contact()
            emergency_contact_contacted = True

        logging.info(f"Try to reconnect in {params.reconnect_time} seconds...")
        time.sleep(params.reconnect_time)
