from mastodon import Mastodon

import params

app_name = ""
url = ""
login = ""
password = ""
with open(params.first_login_file, "r") as f:
    app_name = f.readline().strip()
    login = f.readline().strip()
    password = f.readline().strip()

with open(params.api_url_file, "r") as f:
    url = f.readline().strip()

# Create (once) the application and ask the mastodon instance at url to give us credentials
print(f"creating application {app_name} to access {url}.")
Mastodon.create_app(app_name, api_base_url=url, to_file=params.appCred)

# Use our application to connect to url
print(f"Logging to {url} with application {app_name}.")
mastodon = Mastodon(client_id=params.appCred, api_base_url=url)

# Log in to our specific account at url and also ask for credentials to not have to enter the password everytime
print(f"Logging to the specific account {login} to get credentials.")
mastodon.log_in(login, password, to_file=params.accountCred)
