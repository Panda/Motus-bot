# Debug and log parameters
debug = True

logDir = "data/log"
log = "data/log/log.txt"

reconnect_time = 30

# Login parameters
first_login_file = "data/app_login_password.dat"
appCred = "data/appCred.dat"
accountCred = "data/accountCred.dat"
api_url_file = "data/api_url.dat"

# Emergency contact parameters
emergencyContactFile = "data/emergencyContact.dat"
emergencyMessage = "Bonjour !\nJ'ai rencontré une erreur tout récemment alors je voulais juste te prévenir pour que tu puisses regarder d'où ça vient.\nBon courage !"

# sinceID dire individuellement et une fois par personne en DM "déso j'étais déco, mais maintenent j'ai repris"

sinceID = "data/log/since_id.txt"

wordNumber = "data/wordNumber.dat"

userInfos = "data/users/"

ephemeral = "data/ephemeral/"

congratulation_media = "data/thierry.mp4"
dictionnary = "data/possible.txt"
guessable_words = "data/guessable.txt"
game_data = "data/game.dat"
