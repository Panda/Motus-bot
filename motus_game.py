# import random
# import mastodon
# import Consts
# import  pickle
# from TootHTMLParser import TootHTMLParser
# import threading
# import Log
# import os
# from collections import deque
# import unicodedata

import os
import random
import pickle
import logging
from enum import Enum

import params


class GameType(Enum):
    private = 0
    public = 1


class GameContext:
    def __init__(self, word_to_guess, game_type, status_id, players_progress={}):
        self.word_to_guess = word_to_guess
        self.type = game_type
        self.closed = False
        self.first_status_id = status_id
        self.players_progress = players_progress

    def get_progress(self, player):
        if player in self.players_progress:
            return self.players_progress[player]
        else:
            progress = ""
            for i in range(len(self.word_to_guess)):
                progress += "_"
            progress = self.word_to_guess[0] + progress[1:]
            return progress


class MotusGame:
    def __init__(self):
        self.bot = None
        self.player = None
        random.seed()

        self.dictionnary = None
        self.guessable_words = None

        with open(params.dictionnary) as f:
            self.dictionnary = f.readlines()
            for i in range(len(self.dictionnary)):
                self.dictionnary[i] = self.dictionnary[i].strip().upper()
        with open(params.guessable_words) as f:
            self.guessable_words = f.readlines()

        self.games = []

        if os.path.exists(params.game_data):
            with open(params.game_data, "rb") as f:
                self.games = pickle.load(f)

    def run(self):
        if len(self.games) == 0:
            self.new_game()

    def new_word(self):
        return self.guessable_words[random.randint(0, len(self.guessable_words))].strip()

    def new_game(self, player=None):
        type = GameType.public
        new_word = self.new_word().upper()
        progress = ""
        for i in range(len(new_word)):
            progress += "_"
        progress = new_word[0] + progress[1:]
        text = f"Nouveau mot à deviner ! ({len(new_word)} lettres)\n\n{new_word[0]}"
        for i in range(len(new_word) - 1):
            text += " _"
        if player is not None:
            type = GameType.private
            text = f"""@{player}\n""" + text
        status = self.bot.toot(text, status=None, media=False, is_direct=(player is not None))
        game = GameContext(new_word, type, status["id"], {player: progress})
        logging.info(f"""New game created. Word to find : {new_word} ({type}) => status id : {status["id"]}""")
        self.games.append(game)
        self.save()

    def answer(self, content, status):
        # Can the player answer ? Find the related game.
        player = status["account"]["acct"]
        ancestors = self.bot.api_mastodon.status_context(status["id"])["ancestors"]
        game = None
        if len(ancestors) > 0:
            for potential_game in self.games:
                if potential_game.first_status_id == ancestors[0]["id"]:
                    game = potential_game
                    logging.info("Game found.")
                    break
        if game is None:
            logging.info("No associated game was found.")
            return

        is_direct = (game.type == GameType.private) or status["visibility"] == "direct"

        if game.closed and (
            player not in game.players_progress or game.players_progress[player].strip() == game.word_to_guess.strip()
        ):
            self.bot.toot(
                f"""Le mot a déjà été trouvé et n'est plus jouable désormais.\n\nTu peux jouer avec tout le monde sur le dernier mot que j'ai donné, ou alors me dire "motus" en DM pour avoir un mot à deviner rien qu'à toi.""",
                status=status,
                media=False,
                is_direct=is_direct,
            )
            return

        attempt = ""
        word_to_guess = game.word_to_guess
        words = content.split()
        for word in words:
            if not word.startswith("@"):
                attempt = word
                break

        attempt = attempt.strip().upper()
        if attempt == "":
            return

        logging.info(f"attempt is : {attempt}")

        for letter in attempt:
            if letter not in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                self.bot.toot(
                    f"""Le mot que tu proposes ne doit contenir que les lettres de A à Z.""",
                    status=status,
                    media=False,
                    is_direct=is_direct,
                )
                return

        if len(attempt) != len(word_to_guess):
            self.bot.toot(
                f"""Le mot que tu proposes doit avoir la même taille que le mot à trouver.""",
                status=status,
                media=False,
                is_direct=is_direct,
            )
            return
        elif attempt[0] != word_to_guess[0]:
            self.bot.toot(
                f"""Le mot que tu proposes doit commencer par la même lettre que le mot à trouver.""",
                status=status,
                media=False,
                is_direct=is_direct,
            )
            return
        elif attempt not in self.dictionnary:
            self.bot.toot(
                f"""Ton mot n'est pas dans mon dico !""", status=status, media=False, is_direct=is_direct,
            )
            return
        else:
            logging.info("Attempt is valid")
            progress = game.get_progress(player)
            progress, comparison = self.compare(attempt, progress, word_to_guess)
            game.players_progress[player] = progress

            text = self.get_toot_text(attempt, comparison, progress)
            if attempt == word_to_guess:
                self.bot.toot(f"""{text}\n\nBravo, tu as trouvé !!""", status=status, media=True, is_direct=is_direct)
                closed = game.closed
                game.closed = True
                if game.type == GameType.public and not closed:
                    self.new_game()
            else:
                self.bot.toot(f"""{text}""", status=status, media=False, is_direct=is_direct)

            self.save()

    def save(self):
        with open(params.game_data, "wb") as f:
            pickle.dump(self.games, f, pickle.HIGHEST_PROTOCOL)

    def compare(self, attempt, progress, word_to_guess):
        # COTUTEUR
        # COULEURS
        # CO_X_X_X
        # XX-X-X--
        #
        allocated_indexes = []
        comparison = ""
        for letter in word_to_guess:
            comparison += "_"
        # exact letters
        for index in range(len(attempt)):
            if attempt[index] == word_to_guess[index]:
                comparison = comparison[:index] + word_to_guess[index] + comparison[index + 1 :]
                progress = progress[:index] + word_to_guess[index] + progress[index + 1 :]
                allocated_indexes.append(index)

        # wrong place
        indexes = [i for i in range(len(attempt)) if i not in allocated_indexes]
        for index in indexes:
            for j in range(len(word_to_guess)):
                if attempt[index] == word_to_guess[j] and j not in allocated_indexes:
                    comparison = comparison[:index] + "*" + comparison[index + 1 :]
                    allocated_indexes.append(j)
                    break

        return progress, comparison

    def get_toot_text(self, attempt, comparison, progress):
        text = ""
        for letter in attempt:
            text += ":hacker_" + letter.lower() + ": "
        text += "\n"
        for letter in comparison:
            if letter == "_":
                text += "❎ "
            elif letter == "*":
                text += "↔️ "
            else:
                text += ":hacker_" + letter.lower() + ": "
        text += "\n\n"
        for letter in progress:
            if letter == "_":
                text += "❎ "
            else:
                text += ":hacker_" + letter.lower() + ": "
        return text
